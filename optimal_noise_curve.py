def h(x, delta, eta):
    return (((eta+1)*delta - x)**2 / 12.0 + (x-(eta-1)*delta)**2/4.0 + x**2 + x*(x-(eta-1)*delta))*((eta + 1) * delta - x)

def h_convex_hull(x, delta, eta):
    if eta >= 8 / 3:
        return h(x, delta, eta)
    elif eta >= 1:
        a = (eta - 1) * delta
        b = (5 / 14 * (eta + 2)) * delta

        h_a = h(a, delta, eta)
        h_b = h(b, delta, eta)

        if x < b:
            return (h_b - h_a) * (x - a) / (b - a) + h_a
        else:
            return h(x, delta, eta)

    else:
        a = 0
        b = (3 / 14 * (4 * eta + 1)) * delta

        h_a = h(a, delta, eta)
        h_b = h(b, delta, eta)

        if x < b:
            return (h_b - h_a) * (x - a) / (b - a) + h_a
        else:
            return h(x, delta, eta)
