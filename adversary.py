import numpy as np
from optimal_noise_curve import h_convex_hull
import random
import pandas


def log_adv_utility(mse, prob_acceptance, l):
    return np.log(mse) + l * np.log(prob_acceptance)


class Adversary:
    def __init__(self, lamb, u_ad=log_adv_utility):
        self.utility = u_ad
        self.resolution = 0.001
        self.lamb = lamb
        self.prob_acceptance = None

    def update_best_mse(self, eta, delta):
        util = {}
        mse = {}
        for p in list(np.linspace(0.0001, 1, int(1/self.resolution))):
            mse[p] = 1.0/(4*p*delta) * h_convex_hull(delta*(2*(1-p) + eta - 1), delta, eta) / 2
            util[p] = self.get_utility(mse[p],p)
        best_p = max(util, key=util.get)
        self.prob_acceptance = best_p
        return {'MSE': mse[best_p], 'PA': best_p, 'AD_U': util[best_p]}

    def get_utility(self, mse, prob_acceptance):
        return self.utility(mse, prob_acceptance, self.lamb)

    def sample_from_noise(self, delta, eta):
        if eta >= 8/3:
            z0 = delta*(2*(1-self.prob_acceptance) + eta - 1)
            return random.choices([z0, -z0], weights=(1, 1), k=1)[0]
        elif 8/3 >= eta >= 2:
            if 1-self.prob_acceptance < (24 - 9*eta)/28:
                z1, z2 = (eta - 1)*delta, 5*(eta+2)*delta/14
                w1 = w3 = 0.5 - 14*(1-self.prob_acceptance)/(24-9*eta)
                w2 = w4 = 14*(1-self.prob_acceptance)/(24-9*eta)
                return random.choices([-z1, -z2, z1, z2], weights=(w1, w2, w3, w4), k=1)[0]
            else:
                z0 = delta * (2 * (1 - self.prob_acceptance) + eta - 1)
                return random.choices([z0, -z0], weights=(1, 1), k=1)[0]
        else:
            print("Invalid Eta")
            return -100






